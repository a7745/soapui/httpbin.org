FROM openjdk:18-jdk-slim

ARG SOAPUI_VERSION=5.7.0

RUN apt-get update && apt-get install -y wget && \
    mkdir -p /opt && \
    wget -q -c https://dl.eviware.com/soapuios/$SOAPUI_VERSION/SoapUI-$SOAPUI_VERSION-linux-bin.tar.gz -O - \
    | tar -xzf - -C /opt && \
    ln -s /opt/SoapUI-$SOAPUI_VERSION /opt/SoapUI && \
    rm -rf /var/lib/apt/lists/*

ENV PATH "/opt/SoapUI/bin:$PATH"

ENTRYPOINT [ "testrunner.sh" ]
