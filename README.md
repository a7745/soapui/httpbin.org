# httpbin.org

This is an example API test automation project centering around the [httpbin.org](https://httpbin.org/) API. It uses SoapUI 5.7.0 to perform HTTP resquests to
and validate responses from different endpoints.

The project has a simple [CI pipeline](.gitlab-ci.yml) that contains two jobs:
- `build_docker_image` is executed only on the protected branch `docker`. As the name says, this jobs
  builds a minimal Docker image that comes with Java and SoapUI.
- `run_soapui_tests` is executed on all events. It runs the API tests using SoapUI against a service container.

Feel free to checkout the pipeline jobs and test results in the [CI/CD section](https://gitlab.com/a7745/soapui/httpbin.org/-/pipelines).

## Contributing
This project is mainly a proof of concept for showcasing. However, feedback is highly appreciated! For any ideas or suggestions, you can raise an issue or open a new merge request.

## License
This project is licensed under the [MIT License](LICENSE).
